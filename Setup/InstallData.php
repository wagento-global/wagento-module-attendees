<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */
namespace Wagento\Attendees\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;



class InstallData implements \Magento\Framework\Setup\InstallDataInterface {

    /**
     * @var \Magento\Sales\Setup\SalesSetupFactory
     */
    protected $salesSetupFactory;

    /**
     * @var \Magento\Quote\Setup\QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;


    public function __construct(
        \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory,
        \Magento\Quote\Setup\QuoteSetupFactory $quoteSetupFactory,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /** Install a new Field in the QUOTE and ORDER */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context){


        $quoteInstaller = $this->quoteSetupFactory->create(['resourceName' => 'quote_setup', 'setup' => $setup]);
        $salesInstaller = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $setup]);

        /** We need additional field in the Quote and the Order for Save the Save List from the quote to Order
         * For after to Save to the 'wagento_attendees_list' table */
        $quoteInstaller->addAttribute('quote', 'attendee_list', ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'LENGTH'=>'522']);

        $salesInstaller->addAttribute('order', 'attendee_list', ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 'LENGTH'=>'522']);


        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $fieldList = [
            'price',
            'special_from_date',
            'special_to_date',
            'cost',
            'tier_price',
            'weight',
        ];




    }

}