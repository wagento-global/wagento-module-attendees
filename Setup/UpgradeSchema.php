<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */
namespace Wagento\Attendees\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '2.0.3') < 0) {

            $installer->getConnection()->addColumn(
                $installer->getTable('wagento_attendees_list'),
                'attendees_identity',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    50,
                    array(),
                    'comment' => 'Attendees Identity',
                    'Attendees Identity'
                ]
            );
        }
    }
}