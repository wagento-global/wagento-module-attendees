<?php
/**
 * Copyright © Wagento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wagento\Attendees\Controller;

/**
 * Declarations of core registry keys used by the Attendee module
 *
 */
class RegistryConstants
{
    /**
     * Registry key where current attendee ID is stored
     */
    const CURRENT_ATTENDEE_ID = 'current_id';
}