<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */
namespace Wagento\Attendees\Controller\Attendee;


class Registerattendee extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $_resultRawFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_sessionCheckout;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $_quoteRepository;



    /**
     * Register attendee constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Framework\Json\Helper\Data $helper
     * @param \Magento\Checkout\Model\Session $sessionCheckout
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\Json\Helper\Data $helper,
        \Magento\Checkout\Model\Session $sessionCheckout,
        \Magento\Quote\Model\QuoteRepository $quoteRepository){

        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_resultRawFactory  = $resultRawFactory;
        $this->_helper = $helper;
        $this->_sessionCheckout = $sessionCheckout;
        $this->_quoteRepository = $quoteRepository;
        return parent::__construct($context);
    }
    /**
     */
    public function execute(){
        $attendeesRequest = null;
        $httpBadRequestCode = 400;

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->_resultRawFactory->create();

        try{
            $attendeesRequest = $this->_helper->jsonDecode($this->getRequest()->getContent());
        } catch(\Exception $e){
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        if (!$attendeesRequest || $this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }
        $response = [];
        $response["error"] = false;
        $response["html"]  = '';
        $response["ticketDetail"]  = $attendeesRequest;
        $quote = $this->_sessionCheckout->getQuote();
        $cartTotal = $quote->getSubtotal();

        if($quote && $quote->getId() && count($attendeesRequest) > 0){
            $quote->setData(\Wagento\Attendees\Helper\Data::QUOTE_FIELD, serialize($attendeesRequest));
            $this->_quoteRepository->save($quote);
            /** Generate an html */
            foreach ($attendeesRequest as $key => $attendees){

                foreach ($attendees as $key=> $attendee){

                    if(isset($attendee["attendees_identity"]))
                    {
                        $attendee["attendees_identity"] = $this->getOptionArray(($attendee["attendees_identity"]));
                    }
                    if($cartTotal != '0.0000'){
                        $telephone =  $attendee["telephone"];
                    }else{
                        $telephone = 'N/A';
                    }
//                    if(($attendee["attendees_identity"]) && ($attendee["first_name"]) && ($attendee["email"]) && $telephone)
                    if(($attendee["attendees_identity"]) && ($attendee["first_name"]) && $telephone)
                    {

                        $response["html"] .= implode(", ", $attendee);
                        $response["html"] .= "<br>";
                    }else{
                        $response["error"] = true;
                    }

                }
            }
        }else{
            $response["error"] = true;
        }
        $result = $this->_resultJsonFactory->create();
        return $result->setData($response);
    }

    public function getOptionArray($attendee)
    {
        $array=["1"=>"Merchant","2"=>"Developer"];

        if(array_key_exists($attendee,$array))
        {
            return $array[$attendee];
        }
    }

}