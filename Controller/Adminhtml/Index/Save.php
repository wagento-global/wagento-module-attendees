<?php
/**
 * Copyright © Wagento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wagento\Attendees\Controller\Adminhtml\Index;

use Magento\Framework\Exception\LocalizedException;
use Wagento\Attendees\Controller\Adminhtml\Index;

/**
 * Class Save
 */
class Save extends Index
{
    /**
     * attendee  Save
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $returnToEdit = false;
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            try {
                if (empty($data['id'])) {
                    $data['id'] = null;
                }

                $attendee = $this->attendeeInterfaceFactory->create();

                $this->dataObjectHelper->populateWithArray(
                    $attendee,
                    $data,
                    '\Wagento\Attendees\Api\Data\AttendeeInterface'
                );
                $attendee = $this->attendeeRepository->save($attendee);
                $attendeeId = $attendee->getId();

                $this->_getSession()->unsAttendeeFormData();
                $this->messageManager->addSuccessMessage(__('You saved the attendee.'));
                $returnToEdit = (bool)$this->getRequest()->getParam('back', false);
            } catch (\Magento\Framework\Validator\Exception $exception) {
                $messages = $exception->getMessages();
                if (empty($messages)) {
                    $messages = $exception->getMessage();
                }
                $this->_addSessionErrorMessages($messages);
                $this->_getSession()->setAttendeeFormData($data);
                $returnToEdit = true;
            } catch (LocalizedException $exception) {
                $this->_addSessionErrorMessages($exception->getMessage());
                $this->_getSession()->setAttendeeFormData($data);
                $returnToEdit = true;
            } catch (\Exception $exception) {
                $this->messageManager->addExceptionMessage($exception, __('Something went wrong while saving the attendee.'));
                $this->_getSession()->setAttendeeFormData($data);
                $returnToEdit = true;
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($returnToEdit) {
            if ($attendeeId) {
                $resultRedirect->setPath(
                    'attendees/*/edit',
                    ['id' => $attendeeId, '_current' => true]
                );
            } else {
                $resultRedirect->setPath(
                    'attendees/*/new',
                    ['_current' => true]
                );
            }
        } else {
            $resultRedirect->setPath('attendees/index');
        }
        return $resultRedirect;
    }
}