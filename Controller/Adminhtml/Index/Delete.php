<?php
/**
 * Copyright © Wagento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wagento\Attendees\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;
use Wagento\Attendees\Controller\Adminhtml\Index;

class Delete extends Index
{
    /**
     * Delete attendee action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $attendeeId = $this->initCurrentAttendee();
        if (!empty($attendeeId)) {
            try {
                $this->attendeeRepository->deleteById($attendeeId);
                $this->messageManager->addSuccessMessage(__('You deleted the attendee.'));
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('attendees/index');
    }
}