<?php
/**
 * Copyright © Wagento All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wagento\Attendees\Controller\Adminhtml\Index;

use Wagento\Attendees\Controller\Adminhtml\Index as IndexAction;

class Index extends IndexAction
{
    /**
     * Attendee list action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        /**
         * Set active menu item
         */
        $resultPage->setActiveMenu('Wagento_Attendees::manage');
        $resultPage->getConfig()->getTitle()->prepend(__('Attendees'));

        /**
         * Add breadcrumb item
         */
        $resultPage->addBreadcrumb(__('Attendees'), __('Attendees'));
        $resultPage->addBreadcrumb(__('Manage Attendees'), __('Manage Attendees'));


        $this->_getSession()->unsAttendeeData();
        $this->_getSession()->unsAttendeeFormData();


        return $resultPage;
    }
}
