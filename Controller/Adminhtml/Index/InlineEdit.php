<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */

namespace Wagento\Attendees\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class InlineEdit extends \Magento\Backend\App\Action
{
    /** @var JsonFactory  */
    protected $jsonFactory;
    /**
     * @var \Wagento\Attendees\Model\AttendeeFactory
     */
    private $attendeeFactory;
    /**
     * @var \Wagento\Attendees\Model\ResourceModel\Attendee
     */
    private $attendeeResource;

    /**
     * inlineEdit constructor.
     * @param Context $context
     * @param \Wagento\Attendees\Model\AttendeeFactory $attendeeFactory
     * @param \Wagento\Attendees\Model\ResourceModel\Attendee $attendeeResource
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        \Wagento\Attendees\Model\AttendeeFactory $attendeeFactory,
        \Wagento\Attendees\Model\ResourceModel\Attendee $attendeeResource,
        JsonFactory $jsonFactory
    ) {
        $this->jsonFactory = $jsonFactory;
        $this->attendeeFactory = $attendeeFactory;
        $this->attendeeResource = $attendeeResource;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $id) {
            $attendee = $this->attendeeFactory->create();
            $this->attendeeResource->load($attendee,$id);
            try {
                $attendeeData = $postItems[$id];
                $attendee->setData($attendeeData);
                $this->attendeeResource->save($attendee);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithAttendees($attendee, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithAttendees($attendee, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithAttendees(
                    $attendee,
                    __('Something went wrong while saving the page.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * @param \Wagento\Attendees\Model\Attendee $attendee
     * @param $errorText
     * @return string
     */
    protected function getErrorWithAttendees(\Wagento\Attendees\Model\Attendee $attendee, $errorText)
    {
        return '[AttendeeId ID: ' . $attendee->getAttendeeId() . '] ' . $errorText;
    }
}
