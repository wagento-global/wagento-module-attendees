<?php
/**
 * Copyright © Wagento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wagento\Attendees\Controller\Adminhtml\Index;

use Wagento\Attendees\Controller\Adminhtml\Index as IndexAction;
use Magento\Framework\Controller\ResultFactory;

class MassDelete extends IndexAction
{
    /**
     * @var string
     */
    protected $redirectUrl = '*/*/index';

    /**
     * Execute action
     *
     * @return $this|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $attendeesDeleted = 0;
            foreach ($collection->getAllIds() as $id) {
                $this->attendeeRepository->deleteById($id);
                $attendeesDeleted++;
            }

            if ($attendeesDeleted) {
                $this->messageManager->addSuccessMessage(__('A total of %1 record(s) were deleted.', $attendeesDeleted));
            }
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('attendees/*/index');

            return $resultRedirect;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath($this->redirectUrl);
        }
    }
}