<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wagento\Attendees\Controller\Adminhtml\Index;

use Magento\Framework\Exception\NoSuchEntityException;
use Wagento\Attendees\Controller\Adminhtml\Index;

class Edit extends Index
{
    public function execute()
    {
        $attendeeId = $this->initCurrentAttendee();

        $attendeeData = [];
        $attendee = null;
        $isExistingAttendee = (bool)$attendeeId;
        if ($isExistingAttendee) {
            try {
                $attendee = $this->attendeeRepository->getById($attendeeId);
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while editing the Attendee.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('attnedees/*/index');
                return $resultRedirect;
            }
        }
        $attendeeData['id'] = $attendeeId;
        $this->_getSession()->setAttendeeData($attendeeData);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Wagento_Attendees::manage');
        $this->prepareDefaultAttendeeTitle($resultPage);
        $resultPage->setActiveMenu('Wagento_Attendees::Attendee');
        if ($isExistingAttendee) {
            $resultPage->getConfig()->getTitle()->prepend($attendee->getName());
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New Attendee'));
        }
        return $resultPage;
    }
}