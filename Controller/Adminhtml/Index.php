<?php
/**
 * Copyright © Wagento All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Wagento\Attendees\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Wagento\Attendees\Controller\RegistryConstants;

abstract class Index extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Wagento_Attendees::manage';
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    /**
     * @var \Wagento\Attendees\Api\AttendeeRepositoryInterface
     */
    protected $attendeeRepository;
    /**
     * @var \Wagento\Attendees\Api\Data\AttendeeInterfaceFactory
     */
    protected $attendeeInterfaceFactory;
    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;
    /**
     * @var \Wagento\Attendees\Model\ResourceModel\Attendee\CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Wagento\Attendees\Model\Attendee\Mapper
     */
    protected $attendeeMapper;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Wagento\Attendees\Api\AttendeeRepositoryInterface $attendeeRepository,
        \Wagento\Attendees\Api\Data\AttendeeInterfaceFactory $attendeeInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Wagento\Attendees\Model\ResourceModel\Attendee\CollectionFactory $collectionFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Psr\Log\LoggerInterface $logger,
        \Wagento\Attendees\Model\Attendee\Mapper $attendeeMapper
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->coreRegistry = $coreRegistry;
        $this->attendeeRepository = $attendeeRepository;
        $this->attendeeInterfaceFactory = $attendeeInterfaceFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->logger = $logger;
        $this->attendeeMapper = $attendeeMapper;
    }

    /**
     * Attendees initialization
     *
     * @return string attendees id
     */
    protected function initCurrentAttendee()
    {
        $attendeeId = (int)$this->getRequest()->getParam('id');

        if ($attendeeId) {
            $this->coreRegistry->register(RegistryConstants::CURRENT_ATTENDEE_ID, $attendeeId);
        }

        return $attendeeId;
    }

    /**
     * Prepare attendees default title
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return void
     */
    protected function prepareDefaultAttendeeTitle(\Magento\Backend\Model\View\Result\Page $resultPage)
    {
        $resultPage->getConfig()->getTitle()->prepend(__('Attendees'));
    }

    /**
     * Add errors messages to session.
     *
     * @param array|string $messages
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function _addSessionErrorMessages($messages)
    {
        $messages = (array)$messages;
        $session = $this->_getSession();

        $callback = function ($error) use ($session) {
            if (!$error instanceof Error) {
                $error = new Error($error);
            }
            $this->messageManager->addMessage($error);
        };
        array_walk_recursive($messages, $callback);
    }

    /**
     * Get array with errors
     *
     * @return array
     */
    protected function getErrorMessages()
    {
        $messages = [];
        foreach ($this->getMessageManager()->getMessages()->getItems() as $error) {
            $messages[] = $error->getText();
        }
        return $messages;
    }

    /**
     * Check if errors exists
     *
     * @return bool
     */
    protected function isErrorExists()
    {
        return (bool)$this->getMessageManager()->getMessages(true)->getCount();
    }
}
