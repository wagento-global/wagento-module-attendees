define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Wagento_Attendees/js/model/checkout/attendees-validator'
    ],
    function (Component, additionalValidators, yourValidator) {
        'use strict';
        additionalValidators.registerValidator(yourValidator);
        return Component.extend({});
    }
);