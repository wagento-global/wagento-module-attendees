/**
 * Copyright © 2017 Wagento. All rights reserved.
 */
/*global define*/
define(
    [
        'jquery',
        'underscore',
        'Magento_Ui/js/form/form',
        'Wagento_Attendees/js/model/checkout/attendee-form',
        'Wagento_Attendees/js/action/checkout/register-attendee'
    ],
    function (
        $,
        _,
        Component,
        attendeeFormModel,
        registerAttendee
    ) {
        'use strict';

        /**  Serialize the inputs into array */
        $.fn.serializeControls = function() {

            var data = {};
            function buildInputObject(arr, val) {
                if (arr.length < 1)
                    return val;
                var objkey = arr[0];
                if (objkey.slice(-1) == "]") {
                    objkey = objkey.slice(0,-1);
                }
                var result = {};
                if (arr.length == 1){
                    result[objkey] = val;
                } else {
                    arr.shift();
                    var nestedVal = buildInputObject(arr,val);
                    result[objkey] = nestedVal;
                }
                return result;
            }

            $.each(this.serializeArray(), function() {
                var val = this.value;
                var c = this.name.split("[");
                var a = buildInputObject(c, val);
                $.extend(true, data, a);
            });
            return data;
        };
        var configShow = window.checkoutConfig.showAttendeeList;
        var attendeeList = window.checkoutConfig.attendeeList;
        var productInfo  = window.checkoutConfig.productInfo;

        //var ticketDetail  = window.checkoutConfig.ticketDetail;

        return Component.extend({
            defaults: {
                template: 'Wagento_Attendees/checkout/attendee-form'
            },
            attendeeFormModel: attendeeFormModel,

            /**
             * @return {exports}
             */
            initialize: function () {

                this._super();

                /** Populate the initial values */
                if(configShow === undefined || configShow === null){
                    this.attendeeFormModel.showForm(false);
                }else{
                    this.attendeeFormModel.showForm(configShow);
                }
                /** trick for populate the for Knockout JS */
                var attendeeObject = function(first_name, last_name, email, telephone, totalCart, attendees_identity, company, productId, productName, counter, attendee_counter) {
                    this.first_name = first_name;
                    this.last_name = last_name;
                    this.email = email;
                    this.telephone = telephone;
                    this.totalCart = totalCart;
                    this.attendees_identity = attendees_identity;
                    this.company = company;
                    this.productId = productId;
                    this.productName = productName;
                    this.counter = counter;
                    this.attendee_counter = attendee_counter;
                };

                /** Load information about the attendees */
                var attendee_counter = 1;
                for(var i in attendeeList){
                    var attendees = attendeeList[i], counter = 0;
                    for(var j in attendees){
                        var attendee = attendees[j];
                        this.attendeeFormModel.attendeeList.push(
                            new attendeeObject(
                                attendee.first_name,
                                attendee.last_name,
                                attendee.email,
                                attendee.telephone,
                                attendee.totalCart = window.checkoutConfig.totalsData.base_subtotal,
                                attendee.attendees_identity = window.checkoutConfig.attendeesIdentity,
                                attendee.company,
                                i,
                                productInfo[i],
                                counter++,
                                attendee_counter++
                            )
                        );
                    }
                }
                return this;
            },

            /** Save information about the attendees */
            registerAttendee: function (attendeeForm) {
                /** Capture the fields of the form*/
                var formDataArray = $(attendeeForm).serializeControls();
                if ($(attendeeForm).validation() && $(attendeeForm).validation('isValid')) {
                    attendeeFormModel.formLoader(true);
                    registerAttendee(formDataArray);
                }

            },
            editForm: function(){

                window.checkoutConfig.isAttendeeSave = 0;
                 if(!this.attendeeFormModel.editForm()){
                     this.attendeeFormModel.editForm(true)
                 }
            }
        });
    }
);
