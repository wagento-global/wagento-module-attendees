
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */
define(
    [
        'jquery',
        'ko',
        'mage/storage',
        'Wagento_Attendees/js/model/checkout/attendee-form'
    ],
    function ($,ko, storage, attendeeFormModel) {
        'use strict';

        return function (attendeeData) {

            var attendeeList = window.checkoutConfig.attendeeList;
            var productInfo  = window.checkoutConfig.productInfo;

            return storage.post(
                'attendees/attendee/registerattendee',
                JSON.stringify(attendeeData),
                false
            ).done(
                function (response) {
                      if(!response.error){
                          attendeeFormModel.editForm(false);
                          attendeeFormModel.infoAttendee(response.html);
                          window.checkoutConfig.ticketDetail = response.ticketDetail;
                          window.checkoutConfig.isAttendeeSave = 1;
                      }
                }
            ).always(
                function () {
                    attendeeFormModel.formLoader(false);
                }
            );

        };
    }
);
