/**
 * Copyright © 2017 Wagento. All rights reserved.
 */
/*global define*/
define(
    [
        'jquery',
        'ko'
    ],
    function ($, ko) {
        return {
            attendeeList: ko.observableArray([]),
            showForm: ko.observable(false),
            formLoader: ko.observable(false),
            infoAttendee: ko.observable(""),
            editForm: ko.observable(true)
        };
    }
);
