define(
    [
        'jquery',
        'ko',
        'Wagento_Attendees/js/model/checkout/attendee-form'
    ],
    function ($,ko,attendeeFormModel) {
        'use strict';
        return {

            /**
             * Validate something
             *
             * @returns {boolean}
             */
            validate: function() {

                var isAttendeeSave = window.checkoutConfig.isAttendeeSave;
                var extendattendeesList = window.checkoutConfig.attendeeList;
                var ticketDetail = window.checkoutConfig.ticketDetail;
                var configShow = window.checkoutConfig.showAttendeeList;

                if(ticketDetail != '' && isAttendeeSave == 1) {
                    $.each(ticketDetail, function (index, element) {

                        $.each(element, function (index, element) {
                            // if (element.first_name != '' && element.email != '' && element.telephone != '' && element.attendees_identity != '') {
                            if (element.first_name != '' && element.email != '' && element.attendees_identity != '') {
                                return true;
                            } {
                                alert('Please Register Attendees');
                                return false;
                            }
                        });

                    });
                }
                else if (configShow != false) {
                    alert('Please Register Attendees');
                    return false;
                }

            }
        }
    }
);