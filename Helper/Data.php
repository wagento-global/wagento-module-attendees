<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */
namespace Wagento\Attendees\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {


    CONST CHECKOUT_FIELDS = [
        "first_name" => "",
        "last_name" => "",
        "email" => "",
        "telephone" => "",
        "attendees_identity" => "",
        "company" => ""
    ];

    CONST QUOTE_FIELD = 'attendee_list';

    /**
     * System.xml paths
     */
    CONST XML_PATH_ENABLE_ATTENDEE_STEP = "attendees/general/enable";

    /**
     * check if the module is enable for show an extra step in the checkout
     */
    public function showStepCheckout(){
        return $this->scopeConfig->getValue(self::XML_PATH_ENABLE_ATTENDEE_STEP,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}