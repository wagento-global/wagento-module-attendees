#Used In
Used for Meet Magento India, Magetitan Mexico sites

Wagento Attendees Module for Wagento Event
======================

This Module for Magento® 2 allows to implement For product to made Event Type.

Facts
-----
* version: 2.0.0

Description
-----------
01/12/18

* This module is used to mark products as Event type and  When user purchase qty one or more than one on checkout page, there must be get a fill attendee's below information.
First Name, Last Name, Email and Phone Number
* When user places an order then it will save these data into one custom table. 
  * Table Name: wagento_attendees_list
    * Fields will be
    * Id - auto increment primary key
    * order_id
    * product_id
    * first_name
    * last_name
    * email
    * phone
    * company
    * Ticket hash (For printing barcode)


Requirements
------------
* PHP >= 5.6.5

Compatibility
-------------
* Magento >= 2.1.4

Installation Instructions
-------------------------
The Wagento Attendees module for Magento® 2 is distributed in three formats:
* Drop-In
* [Composer VCS](https://getcomposer.org/doc/05-repositories.md#using-private-repositories)

### Install Source Files ###

The following sections describe how to install the module source files,
depending on the distribution format, to your Magento® 2 instance.


Then navigate to the project root directory and run the following commands:

    composer config repositories.wagento-module-attendees vcs git@bitbucket.org:wagento-global/wagento-module-attendees.git
    composer require wagento/module-attendees:dev-master

#### VCS ####
If you prefer to install the module using [git](https://git-scm.com/), run the
following commands in your project root directory:

    composer config repositories.wagento-module-attendees vcs git@bitbucket.org:wagento-global/wagento-module-attendees.git
    composer require wagento/module-attendees:dev-master

### Enable Module ###
Once the source files are available, make them known to the application:

    ./bin/magento module:enable Wagento_Attendees
    ./bin/magento setup:upgrade

Last but not least, flush cache and compile.

    ./bin/magento cache:flush
    ./bin/magento setup:di:compile

Uninstallation
--------------

The following sections describe how to uninstall the module, depending on the
distribution format, from your Magento® 2 instance.

#### Composer Git ####

To unregister the shipping module from the application, run the following command:

    ./bin/magento module:uninstall --remove-data Wagento_Attendees

This will automatically remove source files and clean up the database.

#### Drop-In ####

To uninstall the module manually, run the following commands in your project
root directory:

    ./bin/magento module:disable Wagento_Attendees
    rm -r app/code/Wagento/Attendees

Features
--------------


#### Admin Settings ####

1) For Enable or Disable on check out Page.

* Go to Admin -> Stores -> configuration -> Wagento -> Attendees Settings -> General Configuration.
   * Enable the Step in the checkout => YES/NO
   
2) For Made any Product as Event type.

* For Create Event type Product -> Go to Admin -> Products ->Catalog -> Add Product from dropdown "Event Product"

3) View and Manage Attendees From admin.

* Go to admin -> Catalog -> Attendees -> Manage Attendees -> "Edit from Action Column"


Developer
---------
* Israel Caceres | [Wagento](https://www.wagento.com/) | israel@wagento.com

License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)