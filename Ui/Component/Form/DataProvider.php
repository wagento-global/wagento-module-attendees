<?php
/**
 * Copyright © Wagento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wagento\Attendees\Ui\Component\Form;

use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    /**
     * @var array
     */
    protected $loadedData;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;
    /**
     * @var \Wagento\Attendees\Model\AttendeeFactory
     */
    protected $attendeeFactory;
    /**
     * @var \Wagento\Attendees\Model\ResourceModel\Attendee
     */
    protected $attendeeResource;
    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $session;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    public function __construct(
         $name,
         $primaryFieldName,
         $requestFieldName,
        \Magento\Framework\Registry $registry,
        \Wagento\Attendees\Model\ResourceModel\Attendee\CollectionFactory $collectionFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Wagento\Attendees\Model\AttendeeFactory $attendeeFactory,
        \Wagento\Attendees\Model\ResourceModel\Attendee $attendeeResource,
        \Magento\Backend\Model\Session $session,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $meta = [],
        array $data = [])
    {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->registry = $registry;
        $this->collection = $collectionFactory->create();
        $this->request = $request;
        $this->attendeeFactory = $attendeeFactory;
        $this->attendeeResource = $attendeeResource;
        $this->session = $session;
        $this->messageManager = $messageManager;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var Attendee $attendee */
        foreach ($items as $attendee) {
            $this->loadedData[$attendee->getId()] = $attendee->getData();
        }

        $data = $this->session->getAttendeeFormData();
        if (!empty($data)) {
            $attendeeId = isset($data['attendee']['id']) ? $data['attendee']['id'] : null;
            $this->loadedData[$attendeeId] = $data;
            $this->session->unsAttendeeFormData();
        }

        return $this->loadedData;

    }
}