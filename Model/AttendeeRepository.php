<?php
/**
* Copyright © 2017 Wagento. All rights reserved.
*/

namespace Wagento\Attendees\Model;

use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;


class AttendeeRepository implements \Wagento\Attendees\Api\AttendeeRepositoryInterface {

    /**
     * @var \Wagento\Attendees\Model\ResourceModel\Attendee
     */
    protected $attendeeResource;

    /**
     * @var \Wagento\Attendees\Model\AttendeeFactory
     */
    protected $attendeeFactory;

    /**
     * @var array
     */
    private $attendees = [];

    /**
     * @param \Wagento\Attendees\Model\ResourceModel\Attendee $ruleResource
     * @param \Wagento\Attendees\Model\AttendeeFactory $ruleFactory
     */
    public function __construct(
        \Wagento\Attendees\Model\ResourceModel\Attendee $attendeeResource,
        \Wagento\Attendees\Model\AttendeeFactory $attendeeFactory
    ) {
        $this->attendeeResource = $attendeeResource;
        $this->attendeeFactory = $attendeeFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Wagento\Attendees\Api\Data\AttendeeInterface $attendee)
    {
        if ($attendee->getId()) {
            $attendee = $this->get($attendee->getId())->addData($attendee->getData());
        }
        try {
            $this->attendeeResource->save($attendee);
            unset($this->attendees[$attendee->getId()]);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        catch (\Exception $e) {
            throw new CouldNotSaveException(__('Unable to save Attendee %1', $attendee->getId()));
        }
        return $attendee;
    }

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        if (!isset($this->attendees[$id])) {
            /** @var \Wagento\Attendees\Model\Attendee $attendee */
            $attendee = $this->attendeeFactory->create();

            /* TODO: change to resource model after entity manager will be fixed */
            $attendee->load($id);
            if (!$attendee->getId()) {
                throw new NoSuchEntityException(__('Attendee with specified ID "%1" not found.', $id));
            }
            $this->attendees[$id] = $attendee;
        }
        return $this->attendees[$id];
    }

    /**
     * {@inheritdoc}
     */



    public function delete(\Wagento\Attendees\Api\Data\AttendeeInterface $attendee)
    {
        try {
            $this->attendeeResource->delete($attendee);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Attendee: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */

    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }


    public function getById($attendeeId)
    {
        $attendee = $this->attendeeFactory->create();
        $this->attendeeResource->load($attendee, $attendeeId);
        if (!$attendee->getId()) {
            throw new NoSuchEntityException(__('Attendee with id "%1" does not exist.', $attendeeId));
        }
        return $attendee;
    }

}