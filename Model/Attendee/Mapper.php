<?php
/**
 * Copyright © Wagento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wagento\Attendees\Model\Attendee;

use Wagento\Attendees\Api\Data\AttendeeInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Convert\ConvertArray;

class Mapper
{
    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(ExtensibleDataObjectConverter $extensibleDataObjectConverter)
    {
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * Convert tree data object to a flat array
     *
     * @param AttendeeInterface $attendee
     * @return array
     */
    public function toFlatArray(AttendeeInterface $attendee)
    {
        $flatArray = $this->extensibleDataObjectConverter->toNestedArray($attendee, [], '\Wagento\Attendees\Api\Data\AttendeeInterface');
        return ConvertArray::toFlatArray($flatArray);
    }
}