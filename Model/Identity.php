<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */

namespace Wagento\Attendees\Model;

use Magento\Framework\Data\OptionSourceInterface;

class Identity implements OptionSourceInterface
{

    const MERCHANT = 1;
    const DEVELOPER = 2;

    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->getAvailableIdentity();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }

    public function getAvailableIdentity()
    {
        return array(self::MERCHANT => __('Merchant'), self::DEVELOPER => __('Developer'));
    }


}