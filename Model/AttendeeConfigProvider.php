<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */

namespace Wagento\Attendees\Model;


class AttendeeConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    protected $helperAttendee;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Wagento\Attendees\Helper\Data $helperAttendee
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->helperAttendee = $helperAttendee;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $eventInfo = $this->getProductEventInfo();

        return [
            'showAttendeeList' => $eventInfo["show"],
            'attendeeList' => $eventInfo["attendeeList"],
            'productInfo' => $eventInfo["productInfo"],
            'ticketDetail' => [],
            'attendeesIdentity' => $eventInfo["attendeesIdentity"],
            'isAttendeeSave' => 0
        ];
    }

    /**
     * Check if exist 2 or more products type Event and get Info
     *
     * @return array
     */
    public function getProductEventInfo(){
        $eventInfo = [];
        $eventInfo["show"] = false;
        $eventInfo["attendeeList"]  = [];
        $eventInfo["productInfo"]   = [];
        $eventInfo["attendeesIdentity"]   = [];
        $quote = $this->checkoutSession->getQuote();

        /** try to retrieve the list of attendee from the Quote */
        $attendeeListField = $quote->getData(\Wagento\Attendees\Helper\Data::QUOTE_FIELD);
        $attendeeList = [];
        /** If the module is not enable not show the form */
        if(!$this->helperAttendee->showStepCheckout()){
            return $eventInfo;
        }

        if(empty($attendeeListField)){
            //TODO: Do nothing
        }else{
            $data = unserialize($attendeeListField);
            if(is_array($data)){
                /** We could not retrieve the List of attendees from the Quote */
                $attendeeList = $data;
            }
        }
        foreach ($quote->getAllVisibleItems() as $item){
            /** if the Qty of the product type event is more than 1 shows the list of attendee */
            if(\Wagento\Attendees\Model\Product\Type\Event::PRODUCT_TYPE == $item->getProduct()->getTypeId() &&
                $item->getQty() >= \Wagento\Attendees\Model\Product\Type\Event::QTY_SHOW_LIST_ATTENDEE){
                $eventInfo["show"] = true;
                $qtyItem = $item->getQty();
                $product = $item->getProduct();
                $productId = $product->getId();
                $itemId = $item->getId();
                $eventInfo["attendeeList"][$itemId] = $this->getAttendeeList($attendeeList, $qtyItem, $productId);
                $eventInfo["productInfo"][$itemId] = $product->getName();
                $eventInfo["attendeesIdentity"] = $this->getOptionArray();
            }
        }

        return $eventInfo;
    }

    /**
     * Get Attendees for populate the Form of attendee_list
     * @param array $attendeeList
     * @param int $qtyItem
     * @param int $productId
     * @return array
     */
    public function getAttendeeList($attendeeList, $qtyItem, $productId){
        $attendees = [];

        $fillArray = true;
        if(isset($attendeeList[$productId])){
            $attendeeListByProduct = $attendeeList[$productId];
            if(is_array($attendeeListByProduct) && count($attendeeListByProduct) > 0){
                /** Trick for populate the Fields with data */
                $qtyField = count($attendeeListByProduct);
                if($qtyField >= $qtyItem){
                    $fillArray = false;
                }else if($qtyField < $qtyItem){
                    $qtyItem = $qtyField - $qtyItem;
                }
                foreach ($attendeeListByProduct as $attendee){
                    array_push($attendees, $attendee);
                }
            }
        }
        if($fillArray){
            for ($i = 0; $i < $qtyItem; $i++){
                /** Fill with a clean array, these serve for show fields in the checkout */
                array_push($attendees, \Wagento\Attendees\Helper\Data::CHECKOUT_FIELDS);
            }
        }
        return $attendees;
    }

    public function getOptionArray()
    {
        return array(
            array("value"=>'1',"type" => __('Merchant')),array("value"=> '2',"type"=> __('Developer'))
        );
    }


}
