<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */
namespace Wagento\Attendees\Model\Product\Type;


class Event extends \Magento\Catalog\Model\Product\Type\AbstractType {


    CONST PRODUCT_TYPE = "event";
    CONST QTY_SHOW_LIST_ATTENDEE = 1;

    /**
     * Check is virtual product
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    public function isVirtual($product)
    {
        return true;
    }

    /**
     * Check that product of this type has weight
     *
     * @return bool
     */
    public function hasWeight()
    {
        return false;
    }
    /**
     * Delete data specific for Event product type
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return void
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {
    }
}