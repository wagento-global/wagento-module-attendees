<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */

namespace Wagento\Attendees\Model;


class Attendee extends \Magento\Framework\Model\AbstractModel
    implements \Wagento\Attendees\Api\Data\AttendeeInterface {

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'wagento_attendees_list';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getAttendee() in this case
     *
     * @var string
     */
    protected $_eventObject = 'attendee';

    protected function _construct(){
        parent::_construct();
        $this->_init('Wagento\Attendees\Model\ResourceModel\Attendee');
        $this->setIdFieldName('id');
    }

    //@codeCoverageIgnoreStart

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstName()
    {
        return $this->getData(self::FIRST_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setFirstName($firstName)
    {
        return $this->setData(self::FIRST_NAME, $firstName);
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName()
    {
        return $this->getData(self::LAST_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setLastName($lastName)
    {
        return $this->setData(self::LAST_NAME, $lastName);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * {@inheritdoc}
     */
    public function getTelephone()
    {
        return $this->getData(self::TELEPHONE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTelephone($telephone)
    {
        return $this->setData(self::TELEPHONE, $telephone);
    }

    /**
     * {@inheritdoc}
     */
    public function getCompany()
    {
        return $this->getData(self::COMPANY);
    }

    /**
     * {@inheritdoc}
     */
    public function setCompany($company)
    {
        return $this->setData(self::COMPANY, $company);
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketHash()
    {
        return $this->getData(self::TICKET_HASH);
    }

    /**
     * {@inheritdoc}
     */
    public function setTicketHash($ticketHash)
    {
        return $this->setData(self::TICKET_HASH, $ticketHash);
    }

    /**
     * {@inheritdoc}
     */
    public function getAttendeesIdentity()
    {
        return $this->getData(self::ATTENDEES_IDENTITY);
    }

    /**
     * {@inheritdoc}
     */
    public function setAttendeesIdentity($attendeesIdentity)
    {
        return $this->setData(self::ATTENDEES_IDENTITY, $attendeesIdentity);
    }

}