<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */

namespace Wagento\Attendees\Model\ResourceModel\Attendee;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{


    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    /**
    /**
     * Set resource model
     *
     * @return void
     * @codeCoverageIgnore
     */
    protected function _construct()
    {
        $this->_init('Wagento\Attendees\Model\Attendee', 'Wagento\Attendees\Model\ResourceModel\Attendee');
    }
}