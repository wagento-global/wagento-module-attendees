<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */

namespace Wagento\Attendees\Api\Data;

interface AttendeeInterface {
    /**#@+
     * Constants defined for keys of data array
     */
    const ID = 'id';

    const ORDER_ID = 'order_id';

    const PRODUCT_ID = 'product_id';

    const FIRST_NAME = 'first_name';

    const LAST_NAME = 'last_name';

    const EMAIL = 'email';

    const TELEPHONE = 'telephone';

    const ATTENDEES_IDENTITY = 'attendees_identity';

    const COMPANY = 'company';

    const TICKET_HASH = 'ticket_hash';

    /**#@-*/

    /**
     * Returns attendee id field
     *
     * @return int|null
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Returns attendee order_id field
     *
     * @return int|null
     */
    public function getOrderId();

    /**
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * Returns attendee product_id field
     *
     * @return int|null
     */
    public function getProductId();

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId);

    /**
     * Returns attendee first_name field
     *
     * @return string|null
     */
    public function getFirstName();

    /**
     * @param int $firstName
     * @return $this
     */
    public function setFirstName($firstName);

    /**
     * Returns attendee last_name field
     *
     * @return string|null
     */
    public function getLastName();

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName);

    /**
     * Returns attendee email field
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email);


    /**
     * Returns attendee telephone field
     *
     * @return string|null
     */
    public function getTelephone();

    /**
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone);

    /**
     * Returns attendee company field
     *
     * @return string|null
     */
    public function getCompany();

    /**
     * @param string $company
     * @return $this
     */
    public function setCompany($company);

    /**
     * Returns attendee ticket_hash field
     *
     * @return string|null
     */
    public function getTicketHash();

    /**
     * @param string $ticketHash
     * @return $this
     */
    public function setTicketHash($ticketHash);

    /**
     * Returns attendee identity field
     *
     * @return string|null
     */
    public function getAttendeesIdentity();

    /**
     * @param string $attendeesIdentity
     * @return $this
     */
    public function setAttendeesIdentity($attendeesIdentity);
}