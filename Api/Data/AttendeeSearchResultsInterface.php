<?php
/**
 * Copyright © Wagento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Wagento\Attendees\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface AttendeeSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get attendee list.
     *
     * @return \Wagento\Attendees\Api\Data\AttendeeInterface[]
     */
    public function getItems();

    /**
     * Set attendee list.
     *
     * @param \Wagento\Attendees\Api\Data\AttendeeInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}