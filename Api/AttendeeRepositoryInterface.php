<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */

namespace Wagento\Attendees\Api;

/**
 * Interface CatalogRuleRepositoryInterface
 * @api
 */
interface AttendeeRepositoryInterface
{
    /**
     * @param \Wagento\Attendees\Api\Data\AttendeeInterface $attendee
     * @return \Wagento\Attendees\Api\Data\AttendeeInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Wagento\Attendees\Api\Data\AttendeeInterface $attendee);

    /**
     * @param int $id
     * @return \Wagento\Attendees\Api\Data\AttendeeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */

    public function getById($attendeeId);

    /**
     * Retrieve attendees which match a specified criteria.
     *
     * This call returns an array of objects, but detailed information about each object’s attributes might not be
     * included. See http://devdocs.magento.com/codelinks/attributes.html#AttendeeRepositoryInterface to determine
     * which call to use to get detailed information about all attributes for an object.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Wagento\Attendees\Api\Data\AttendeeSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function get($id);

    /**
     * @param \Wagento\Attendees\Api\Data\AttendeeInterface $attendee
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Wagento\Attendees\Api\Data\AttendeeInterface $attendee);

    /**
     * @param int $id
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($id);
}
