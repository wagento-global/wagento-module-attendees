<?php
/**
 * Copyright © 2017 Wagento. All rights reserved.
 */

namespace Wagento\Attendees\Observer;

use Magento\Quote\Model\Quote\ItemFactory;

class SaveAttendeeList implements \Magento\Framework\Event\ObserverInterface {


    /**
     * @var \Wagento\Attendees\Api\Data\AttendeeInterfaceFactory
     */
    protected $_attendeeInterfaceFactory;

    /**
     * @var \Wagento\Attendees\Api\AttendeeRepositoryInterface
     */
    protected $_attendeeRepository;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $_quoteRepository;
    /**
     * @var ItemFactory
     */
    private $itemFactory;
    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\Item
     */
    private $itemResource;

    /**
     * SaveAttendeeList constructor.
     * @param \Wagento\Attendees\Api\Data\AttendeeInterfaceFactory $attendeeInterfaceFactory
     * @param \Wagento\Attendees\Api\AttendeeRepositoryInterface $attendeeRepository
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param ItemFactory $itemFactory
     * @param \Magento\Quote\Model\ResourceModel\Quote\Item $itemResource
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Wagento\Attendees\Api\Data\AttendeeInterfaceFactory $attendeeInterfaceFactory,
        \Wagento\Attendees\Api\AttendeeRepositoryInterface $attendeeRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Quote\Model\Quote\ItemFactory $itemFactory,
        \Magento\Quote\Model\ResourceModel\Quote\Item $itemResource,
        \Psr\Log\LoggerInterface $logger)
    {
        $this->_attendeeInterfaceFactory = $attendeeInterfaceFactory;
        $this->_attendeeRepository = $attendeeRepository;
        $this->_orderRepository = $orderRepository;
        $this->_logger = $logger;
        $this->_quoteRepository = $quoteRepository;
        $this->itemFactory = $itemFactory;
        $this->itemResource = $itemResource;
    }
    /**
    */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds) || count($orderIds) != 1) {
            return $this;
        }
        $orderId = current($orderIds);
        try{
            $order = $this->_orderRepository->get($orderId);
            $quoteId = $order->getQuoteId();
            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $this->_quoteRepository->get($quoteId);
            $attendeeList = $quote->getData(\Wagento\Attendees\Helper\Data::QUOTE_FIELD);
            if($attendeeList && !empty($attendeeList)){
                $attendeeList = unserialize($attendeeList);
                if(is_array($attendeeList)){
                    /** $key is the product Id*/
                    foreach ($attendeeList as $key => $attendees){
                            $itemId = $key;
                            $item = $this->itemFactory->create();
                            $this->itemResource->load($item, $itemId);
                            $productId = $item->getProductId();
                            /** $attendee is an array */
                            foreach ($attendees as $attendee){
                                /** @var \Wagento\Attendees\Api\Data\AttendeeInterface $attendeeModel */
                                $attendeeModel = $this->_attendeeInterfaceFactory->create();
                                $attendeeModel->setData($attendee);
                                $attendeeModel->setProductId($productId);
                                $attendeeModel->setOrderId($orderId);
                                $this->_attendeeRepository->save($attendeeModel);
                            }
                    }
                }
            }
        }catch (\Exception $e){
            $this->_logger->critical($e->getMessage());
        }
        return $this;
    }
}